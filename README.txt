About TracSpamFilter
====================

TracSpamFilter is a plugin for Trac (http://trac.edgewall.com/) that provides
an infrastructure for detecting and rejecting spam (or other forms of
illegitimate/unwanted content) in submitted content.

This plugin requires Trac 0.10 or later.


More Information
----------------

See the website at

  <http://projects.edgewall.com/trac/wiki/SpamFilter>
